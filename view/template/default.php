<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/png" href="/images/favicon.png" />

		<title>Jeu Javascript | Transition et @Keyframe</title>

		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://unpkg.com/popper.js@1.14.6/dist/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

		<link rel="stylesheet" type="text/css" href="/style/css/style.css">
		<script type="text/javascript" src="/js/script.js"></script>
	</head>
	<body class="no-padding">
		<header>
			<h1>Jeu Javascript | Transition et @Keyframe</h1>
			<nav class="navbar navbar-expand-sm navbar-light justify-content-center">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-center" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="/">Cours</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/jeu1">Jeu 1</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/jeu2">Jeu 2</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/jeu3">Jeu 3</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		
		<?=$content?>
		
		<footer>
			<div class="bandeau">
				<p>Site créé par <a href="http://flavien-panunzio.tk/" target="_blank">Flavien Panunzio</a> | &copy 2018 Panunzio Flavien</p>
			</div>
		</footer>
	</body>
</html>