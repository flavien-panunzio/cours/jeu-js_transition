<h1>Jeu JavaScript -> Transition</h1>

<p>Jeu réalisé durant ma deuxième année de DUT MMI.<br/>
<strong>Projet réalisé en 10 heures.</strong></p>

<p>Le sujet étais de faire un site web éducatif en JavaScript.<br/>
J'ai donc décidé de faire ce jeu dans l'optique d'apprendre la propriétés CSS "transition".</p>

<p>Le jeu se déroule en deux parties :</p>
<ul>
    <li>Une première partie de cours avec des exemples</li>
    <li>Une seconde partie de développement pour apprendre à manipuler ces propriétés</li>
</ul>