<?php
	ob_start();
	$url=$_SERVER['REQUEST_URI'];
	$array=['/index','/jeu1','/jeu2','/jeu3','/404'];
	if ($url==="/")
		$url='/index';
	$template='/default';
	if (in_array($url, $array))
		$view=$url;
	else{
		header("HTTP/1.0 404 Not Found");
		header("Location: /404");
	}
	require (__DIR__.'/view/pages' . $view . '.html');
	$content = ob_get_clean();
	require (__DIR__.'/view/template' . $template . '.php');